package org.ksga.homework001.controller;

import org.ksga.homework001.model.Category;
import org.ksga.homework001.model.User;
import org.ksga.homework001.model.dto.CategoryDto;
import org.ksga.homework001.model.dto.NewUser;
import org.ksga.homework001.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    //    ----------------- fetch ----------------------
    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getAllCategory(@RequestParam Optional<Integer> page) {

        if (page.isEmpty()) page = Optional.of(0);

        Pageable pageable = PageRequest.of(page.get(), 5);

        Page<Category> categories = categoryService.findAll(pageable);
        Map map = new HashMap();

        map.put("data", categories.getContent());
        map.put("pagable", categories.getPageable());
        map.put("totalPage", categories.getTotalPages());
        map.put("totalElement", categories.getTotalElements());

        return ResponseEntity.ok().body(map);
    }


    //    ---------------- delete---------------------
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, String>> deleteCategoryById(@PathVariable int id) {

        Map map = new HashMap();
        map.clear();
        if (categoryService.existsById(id)) {
            categoryService.deleteById(id);
            map.put("status", "Ok");
            map.put("message", "Category was deleted successfully");
        } else {
            map.put("status", "Failed");
            map.put("message", "Category id does not exsist!");
        }

        return ResponseEntity.ok().body(map);
    }

    //    --------------- update --------------
    @PutMapping("/{id}")
    public ResponseEntity<Map<String, Object>> updateCategory(@PathVariable int id,@RequestBody CategoryDto categoryDto) {
        Map map = new LinkedHashMap();
        if(!categoryService.existsById(id)){
            map.put("satus", "failed");
            map.put("message","Can not update category");
            map.put("description","Can not found this category in database or category has not been initialized.");
            return ResponseEntity.badRequest().body(map);
        }else{
            if(categoryDto.getName().isEmpty()||categoryDto.getName().isBlank()){
                map.put("satus", "failed");
                map.put("message","categoryName can not empty!.");
                return ResponseEntity.badRequest().body(map);
            }else{
                Category category = categoryService.getById(id);
                category.setName(categoryDto.getName());
                category = categoryService.saveAndFlush(category);
                map.put("staus","Ok");
                map.put("message", category.getName()+" created successfully");
                map.put("data",categoryDto);
                return ResponseEntity.ok().body(map);
            }
        }
    }

    //    ------------- insert -------------------
    @PostMapping("")
    public ResponseEntity<Map<String, Object>> createCategory(@RequestBody CategoryDto categoryDto) {
        Map map = new LinkedHashMap();
        if(categoryDto.getName().isEmpty()||categoryDto.getName().isBlank()){
            map.put("satus", "failed");
            map.put("message","categoryName can not empty!.");
            return ResponseEntity.badRequest().body(map);
        }else{
            Category category = new Category();
            category.setName(categoryDto.getName());
            category = categoryService.saveAndFlush(category);
            map.put("staus","Ok");
            map.put("message", category.getName()+" created successfully");
            map.put("data",categoryDto);
            return ResponseEntity.ok().body(map);
        }

    }
}
