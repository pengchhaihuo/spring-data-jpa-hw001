package org.ksga.homework001.controller;

import org.ksga.homework001.model.Article;
import org.ksga.homework001.model.Category;
import org.ksga.homework001.model.User;
import org.ksga.homework001.model.dto.ArticleDto;
import org.ksga.homework001.service.ArticleService;
import org.ksga.homework001.service.CategoryService;
import org.ksga.homework001.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("article")
public class ArticleController {

    @Autowired
    ArticleService articleService;
    @Autowired
    UserService userService;
    @Autowired
    CategoryService categoryService;

    //---------------------- fetch all --------------------------------------
    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getAllArticle(@RequestParam Optional<Integer> page) {
        if (page.isEmpty()) page = Optional.of(0);

        Pageable pageable = PageRequest.of(page.get(), 5);

        Page<Article> articles = articleService.findAll(pageable);

        Map map = new HashMap();

        map.put("data", articles.getContent());
        map.put("pagable", articles.getPageable());
        map.put("totalPage", articles.getTotalPages());
        map.put("totalElement", articles.getTotalElements());

        return ResponseEntity.ok().body(map);
    }

    //    ------------------------- delete  -------------------------------
    @DeleteMapping("{id}")
    public ResponseEntity<Map<String, String>> deleteArticle(@PathVariable int id) {

        Map map = new HashMap();
        map.clear();
        if (articleService.existsById(id)) {
            articleService.deleteById(id);
            map.put("status", "Ok");
            map.put("message", "Article was deleted successfully");
        } else {
            map.put("status", "Failed");
            map.put("message", "Article id does not exsist!");
        }

        return ResponseEntity.ok().body(map);
    }

    //    --------------- update --------------
    @PutMapping("/{id}")
    public ResponseEntity<Map<String, Object>> updateArticle(@PathVariable int id, @RequestBody ArticleDto newArticle) {
        Map map = new HashMap();
        if (!articleService.existsById(id)) {
            map.put("status", "Failed");
            map.put("message", "Article id does not exsist!");
            return ResponseEntity.badRequest().body(map);
        } else {
            if ((newArticle.getTitle().isEmpty())||( newArticle.getTitle().isBlank())) {
                map.put("status", "Failed");
                map.put("message", "Article title can not be empty!");
                return ResponseEntity.badRequest().body(map);
            } else if ((newArticle.getDescription().isEmpty()) || (newArticle.getDescription().isBlank())) {
                map.put("status", "Failed");
                map.put("message", "Article description can not be empty!");
                return ResponseEntity.badRequest().body(map);
            } else if (newArticle.getCategoryId()==0) {
                map.put("status", "Failed");
                map.put("message", "Category id can not be empty or zero!");
                return ResponseEntity.badRequest().body(map);
            } else if (newArticle.getUserId()==0) {
                map.put("status", "Failed");
                map.put("message", "User id can not be empty or zero!");
                return ResponseEntity.badRequest().body(map);
            } else {
                Article article = articleService.getArticleById(id);
                if (!categoryService.existsById(newArticle.getCategoryId())) {
                    map.put("satus", "failed");
                    map.put("message", "Can not update article");
                    map.put("description", "Can not found this category or category has not been initialized.");
                    return ResponseEntity.badRequest().body(map);
                } else if (!userService.existsById(newArticle.getUserId())) {
                    map.put("satus", "failed");
                    map.put("message", "Can not update article");
                    map.put("description", "Can not found this user or user has not been initialized.");
                    return ResponseEntity.badRequest().body(map);
                } else {
                    Category category = categoryService.getCategoryById(newArticle.getCategoryId());
                    User user = userService.getUsersById(newArticle.getUserId());
                    article.setTitle(newArticle.getTitle());
                    article.setDescription(newArticle.getDescription());
                    article.setUser(user);
                    article.setCategory(category);
                    article = articleService.saveAndFlush(article);
                    map.put("status", "ok");
                    map.put("message", "article was updated successfull");
                    map.put("data", article);
                    return ResponseEntity.ok().body(map);
                }
            }
        }
    }

    //    ------------- insert -------------------
    @PostMapping("")
    public ResponseEntity<Map<String, Object>> createArticle(@RequestBody ArticleDto newArticle) {
        Map map = new HashMap();
        Article article = new Article();

        if ((newArticle.getTitle().isEmpty())||( newArticle.getTitle().isBlank())) {
            map.put("status", "Failed");
            map.put("message", "Article title can not be empty!");
            return ResponseEntity.badRequest().body(map);
        } else if ((newArticle.getDescription().isEmpty()) || (newArticle.getDescription().isBlank())) {
            map.put("status", "Failed");
            map.put("message", "Article description can not be empty!");
            return ResponseEntity.badRequest().body(map);
        } else if (newArticle.getUserId()==0) {
            map.put("status", "Failed");
            map.put("message", "User id can not be empty or zero!");
            return ResponseEntity.badRequest().body(map);
        } else if (newArticle.getCategoryId()==0) {
            map.put("status", "Failed");
            map.put("message", "Category id can not be empty or zero!");
            return ResponseEntity.badRequest().body(map);
        } else {
            if (!categoryService.existsById(newArticle.getCategoryId())) {
                map.put("satus", "failed");
                map.put("message", "Can not add article");
                map.put("description", "Can not found this category or category has not been initialized.");
                return ResponseEntity.badRequest().body(map);
            } else if (!userService.existsById(newArticle.getUserId())) {
                map.put("satus", "failed");
                map.put("message", "Can not add article");
                map.put("description", "Can not found this user or user has not been initialized.");
                return ResponseEntity.badRequest().body(map);
            } else {
                Category category = categoryService.getCategoryById(newArticle.getCategoryId());
                User user = userService.getUsersById(newArticle.getUserId());
                article.setTitle(newArticle.getTitle());
                article.setDescription(newArticle.getDescription());
                article.setUser(user);
                article.setCategory(category);
                article = articleService.saveAndFlush(article);
                map.put("status", "ok");
                map.put("message", "article was created successfull");
                map.put("data", article);
                return ResponseEntity.ok().body(map);
            }
        }
    }

    //    ------------ get by user -----------------
    @GetMapping("/user/{userId}")
    public ResponseEntity<Map<String, Object>> getAllArticlesCreateByUser
    (@PathVariable int userId, @RequestParam Optional<Integer> page) {
        Map map = new HashMap();
        if(!userService.existsById(userId)){
            map.put("satus", "failed");
            map.put("message", "Can not add article");
            map.put("description", "Can not found this user or user has not been initialized.");
            return ResponseEntity.badRequest().body(map);
        }else{
            if (page.isEmpty()) page = Optional.of(0);
            Pageable pageable = PageRequest.of(page.get(), 5);
            Page<Article> articles = articleService.getArticleByUserId(userId, pageable);
            map.put("data", articles.getContent());
            map.put("totalPage", articles.getTotalPages());
            map.put("totalElement", articles.getTotalElements());
            return ResponseEntity.ok().body(map);
        }
    }


    //    ------------ get by category -----------------
    @GetMapping("/category/{categoryId}")
    public ResponseEntity getAllArticlesFromCategory(@PathVariable int categoryId, @RequestParam Optional<Integer> page) {
        Map map = new HashMap();
        if (!categoryService.existsById(categoryId)) {
            map.put("satus", "failed");
            map.put("message", "Can not add article");
            map.put("description", "Can not found this category or category has not been initialized.");
            return ResponseEntity.badRequest().body(map);
        } else {
            if (page.isEmpty()) page = Optional.of(0);
            Pageable pageable = PageRequest.of(page.get(), 5);
            Page<Article> categories = articleService.getArticleByCategoryId(categoryId, pageable);
            map.put("data", categories.getContent());
            map.put("totalPage", categories.getTotalPages());
            map.put("totalElement", categories.getTotalElements());
            return ResponseEntity.ok().body(map);
        }
    }
}
