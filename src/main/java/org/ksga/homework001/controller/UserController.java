package org.ksga.homework001.controller;

import org.ksga.homework001.model.dto.NewUser;
import org.ksga.homework001.model.User;
import org.ksga.homework001.model.dto.UserDto;
import org.ksga.homework001.model.dto.UserUpdate;
import org.ksga.homework001.repository.RoleRepository;
import org.ksga.homework001.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UserService userService;
    @Autowired
    RoleRepository roleRepository;

    //  ------------------------- fetch user ------------------------------
    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getAllUser(@RequestParam Optional<Integer> page) {

        if (page.isEmpty()) page = Optional.of(0);

        Pageable pageable = PageRequest.of(page.get(), 5);
        Page<User> users = userService.findAll(pageable);
        List<UserDto> userDtos = new ArrayList<>();
        userDtos.clear();
        users.forEach(user -> {
            userDtos.add(modelMapper.map(user, UserDto.class));
//            System.out.println(userDtos);
        });

        Map map = new HashMap();

        map.put("data", userDtos);
        map.put("pagable", users.getPageable());
        map.put("totalPage", users.getTotalPages());
        map.put("totalElement", users.getTotalElements());

        return ResponseEntity.ok().body(map);
    }

    //    -------------------------- delete ---------------------------------
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, String>> deleteUserById(@PathVariable int id) {

        Map map = new HashMap();
        map.clear();
        if (userService.existsById(id)) {
            userService.deleteById(id);
            map.put("status", "Ok");
            map.put("message", "User was deleted successfully");
            return ResponseEntity.ok().body(map);
        } else {
            map.put("status", "Failed");
            map.put("message", "User id does not exsist!");
            return ResponseEntity.badRequest().body(map);
        }

    }

    //    --------------- update --------------
    @PutMapping("/{userId}")
    public ResponseEntity<Map<String, Object>> updateUser(@PathVariable int userId, @RequestBody UserUpdate userUpdate) {
        Map map = new LinkedHashMap();
        if (!userService.existsById(userId)) {
            map.put("status", "failed");
            map.put("message", "Can not find this user");
            return ResponseEntity.badRequest().body(map);
        } else {
            User user = userService.getUsersById(userId);
            if((userUpdate.getUsername().isEmpty())||userUpdate.getUsername().equals(" ")){
                map.put("status", "failed");
                map.put("message", "Username can not empty.");
                return ResponseEntity.badRequest().body(map);
            }else if((userUpdate.getPassword().isEmpty())||userUpdate.getPassword().equals(" ")){
                map.put("status", "failed");
                map.put("message", "Password can not empty.");
                return ResponseEntity.badRequest().body(map);
            }else{
                user.setUsername(userUpdate.getUsername());
                user.setPassword(userUpdate.getPassword());
                user = userService.saveAndFlush(user);
                map.put("status", "OK");
                map.put("message", user.getUsername() + " was updated successfully.");
                map.put("data", userService.getUsersById(userId));
                return ResponseEntity.ok().body(map);
            }
        }
    }

    //    ------------- insert -------------------
    @PostMapping("")
    public ResponseEntity<Map<String, Object>> createUser(@RequestBody NewUser newUser) {
        Map map = new LinkedHashMap();
        try{
            if (!roleRepository.existsById(newUser.getRoleId())) {
                map.put("status", "Failed");
                map.put("message", "Can not create user");
                map.put("Description", "Role Id has not in Database");
                return ResponseEntity.badRequest().body(map);
            }
            else {
                if((newUser.getUsername().isEmpty())||newUser.getUsername().equals(" ")){
                    map.put("status", "failed");
                    map.put("message", "Username can not empty.");
                    return ResponseEntity.badRequest().body(map);
                }else if((newUser.getPassword().isEmpty())||newUser.getPassword().equals(" ")){
                    map.put("status", "failed");
                    map.put("message", "Password can not empty.");
                    return ResponseEntity.badRequest().body(map);
                }else {
                    User user = new User();
                    user.setUsername(newUser.getUsername());
                    user.setPassword(newUser.getPassword());
                    user = userService.saveAndFlush(user);
                    userService.addRoleUser(user.getId(), newUser.getRoleId());
                    map.put("status", "Ok");
                    map.put("message", user.getUsername() + " was created successfully");
                    map.put("userId", user.getId());
                    return ResponseEntity.ok().body(map);
                }
            }
        }catch (Exception jex){
            map.put("status", "Failed");
            map.put("message", "Can not create user");
            map.put("Description", jex.getMessage());
            return ResponseEntity.badRequest().body(map);
        }
    }

    //    --------------- get all user by specific role ---------------------
    @GetMapping("/role/{roleId}")
    public ResponseEntity<Map<String, Object>> createUser(
            @PathVariable int roleId, @RequestParam Optional<Integer> page) {
        Map map = new HashMap();
        if(!roleRepository.existsById(roleId)){
            map.put("status","Failed");
            map.put("message","Can not found this role or role has not been initialized.");
            return ResponseEntity.badRequest().body(map);
        }else {
            if (page.isEmpty()) page = Optional.of(0);
            Pageable pageable = PageRequest.of(page.get(), 5);
            Page<User> users = userService.getUsersByRoleId(roleId, pageable);
            map.put("data", users.getContent());
            map.put("totalPage", users.getTotalPages());
            map.put("totalElement", users.getTotalElements());
            return ResponseEntity.ok().body(map);
        }
    }
}
