package org.ksga.homework001.repository;

import org.ksga.homework001.model.Article;
import org.ksga.homework001.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer>, PagingAndSortingRepository<Category,Integer> {

    Category getCategoryById(int id);
}
