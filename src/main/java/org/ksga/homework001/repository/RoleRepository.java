package org.ksga.homework001.repository;

import org.ksga.homework001.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Roles,Integer>{

    Roles getRolesById(int id);
}
