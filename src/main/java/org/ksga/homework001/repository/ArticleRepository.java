package org.ksga.homework001.repository;

import org.ksga.homework001.model.Article;
import org.ksga.homework001.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer>, PagingAndSortingRepository<Article,Integer> {

    Article getArticleById(int id);

    Page<Article> getArticleByCategoryId(int categoryId, Pageable pageable);
    List<Article> getArticleByCategoryId(int categoryId);

    Page<Article> getArticleByUserId(int userId, Pageable pageable);
    List<Article> getArticleByUserId(int userId);
}
