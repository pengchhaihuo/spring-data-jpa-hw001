package org.ksga.homework001.repository;

import org.ksga.homework001.model.Article;
import org.ksga.homework001.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Integer>, PagingAndSortingRepository<User,Integer> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO user_role VALUES (:userId,:roleId)", nativeQuery = true)
  void addRoleUser(int userId, int roleId);

    @Query(value = "SELECT * FROM users as u INNER JOIN user_role as ur\n" +
            "ON u.id= ur.user_id WHERE ur.role_id = :id ", nativeQuery = true)
    Page<User> getUsersByRoleId(int id, Pageable pageable);

    User getUsersById(int id);
}
