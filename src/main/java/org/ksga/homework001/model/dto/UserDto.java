package org.ksga.homework001.model.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ksga.homework001.model.Roles;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    int id;
    String username;
    private List<Roles> roles ;
}
