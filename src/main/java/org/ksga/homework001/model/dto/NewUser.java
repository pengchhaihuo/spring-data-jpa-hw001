package org.ksga.homework001.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NewUser {
    String username;
    String password;
    int roleId;

}
