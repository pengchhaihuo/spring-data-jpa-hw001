package org.ksga.homework001.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
@Data
public class UserUpdate {
    String username;
    String password;
}
