package org.ksga.homework001.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "articles")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String title;
    String description;

    @ManyToOne()
    @JsonManagedReference
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    private User user;

    @ManyToOne()
    @JoinColumn(name= "category_id", referencedColumnName = "id")
    private Category category;


}
