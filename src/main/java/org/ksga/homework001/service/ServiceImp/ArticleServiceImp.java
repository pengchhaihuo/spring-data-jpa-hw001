package org.ksga.homework001.service.ServiceImp;

import org.ksga.homework001.model.Article;
import org.ksga.homework001.model.User;
import org.ksga.homework001.repository.ArticleRepository;
import org.ksga.homework001.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    ArticleRepository articleRepository;

    @Override
    public Article getArticleById(int id) {
        return articleRepository.getArticleById(id);
    }

    @Override
    public Page<Article> getArticleByCategoryId(int categoryId, Pageable pageable) {
        return articleRepository.getArticleByCategoryId(categoryId,pageable);
    }

    @Override
    public Page<Article> getArticleByUserId(int userId, Pageable pageable) {
        return articleRepository.getArticleByUserId(userId,pageable);
    }

    @Override
    public List<Article> getArticleByUserId(int userId) {
        return articleRepository.getArticleByUserId(userId);
    }

    @Override
    public List<Article> getArticleByCategoryId(int userId) {
        return articleRepository.getArticleByCategoryId(userId);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public List<Article> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<Article> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public <S extends Article> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Article> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends Article> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<Article> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Integer> integers) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Article getOne(Integer integer) {
        return null;
    }

    @Override
    public Article getById(Integer integer) {
        return null;
    }

    @Override
    public <S extends Article> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Article> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Page<Article> findAll(Pageable pageable) {
        return articleRepository.findAll(pageable);
    }

    @Override
    public <S extends Article> S save(S entity) {
        return null;
    }

    @Override
    public Optional<Article> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Article entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Integer> integers) {

    }

    @Override
    public void deleteAll(Iterable<? extends Article> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Article> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Article> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Article> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Article> boolean exists(Example<S> example) {
        return false;
    }
}
