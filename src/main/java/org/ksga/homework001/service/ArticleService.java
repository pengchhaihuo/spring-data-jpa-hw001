package org.ksga.homework001.service;

import org.ksga.homework001.model.Article;
import org.ksga.homework001.repository.ArticleRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ArticleService extends ArticleRepository {

}
