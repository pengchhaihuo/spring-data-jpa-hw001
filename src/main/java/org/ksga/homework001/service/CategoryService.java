package org.ksga.homework001.service;

import org.ksga.homework001.model.Category;
import org.ksga.homework001.repository.CategoryRepository;

import java.util.List;

public interface CategoryService extends CategoryRepository {

}
